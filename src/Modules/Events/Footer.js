import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import styles from './FooterCss';


export default class Footer extends React.Component {
    render() {
        return (
            <View style={styles.footerWrapper}>
            	<TouchableOpacity>
                	<Image style={styles.hamBurgerIcon} source={require('../../Assets/Images/hamburger.png')} />
                </TouchableOpacity>
                <TouchableOpacity>
                	<Text style={styles.createText}>+ Create Event</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
