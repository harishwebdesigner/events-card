import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import styles from './indexCss';
import Header from './Header';
import EventHeader from './EventHeader';
import EventCarousel from './EventCarousel';
import Footer from './Footer';


export const CAROUSEL_DATA = [
    {
        title: 'Just For Laughs',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        location: 'Berlin, Germany',
        eventTime: 'Apr 19th, 2018',
        illustration: 'https://cf.ltkcdn.net/teens/images/std/204995-679x453-Red-White-and-Blue.jpg'
    },
    {
        title: 'Film Festival',
        subtitle: 'Lorem ipsum dolor sit amet',
        location: 'Berlin, Germany',
        eventTime: 'Apr 19th, 2018',
        illustration: 'https://static.standard.co.uk/s3fs-public/thumbnails/image/2017/08/07/12/nightlife0708a.jpg?w968h681'
    },
    {
        title: 'Niagara Festival',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
        location: 'Berlin, Germany',
        eventTime: 'Apr 19th, 2018',
        illustration: 'https://cached.imagescaler.hbpl.co.uk/resize/scaleWidth/815/cached.offlinehbpl.hbpl.co.uk/news/SUC/coke-20160819012857986.jpg'
    },
    {
        title: 'Night It Up',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        location: 'Berlin, Germany',
        eventTime: 'Apr 19th, 2018',
        illustration: 'http://vitriviera.com/static/media/pre.17254662.jpg'
    },
    {
        title: 'Daffodil Ball',
        subtitle: 'Lorem ipsum dolor sit amet',
        location: 'Berlin, Germany',
        eventTime: 'Apr 19th, 2018',
        illustration: 'https://www.quietevents.com/wp-content/uploads/2018/03/silent-disco-costume-party-theme-header.jpg'
    },
    {
        title: 'Juno Awards',
        subtitle: 'Lorem ipsum dolor sit amet',
        location: 'Berlin, Germany',
        eventTime: 'Apr 19th, 2018',
        illustration: 'http://www.wallpaperdx.com/images/disco-club-girl-dance-exercise-hair-glasses-lights-full-hd-wallpaper.jpg'
    },
    {
        title: 'C2 Montreal',
        subtitle: 'Lorem ipsum dolor sit amet',
        location: 'Berlin, Germany',
        eventTime: 'Apr 19th, 2018',
        illustration: 'https://cdn.shutterstock.com/shutterstock/videos/8325157/thumb/1.jpg'
    }
];

export default class Message extends React.Component {
    constructor(props){
        super(props);
        this.onSnapToItem = this.onSnapToItem.bind(this);
        this.state = {
            currentEvent: CAROUSEL_DATA.length
        }
    }

    onSnapToItem(index){
        const self = this;
        this.setState({
            currentEvent: index
        })
        self.headerContainer.scrollTo({x: 0, y: 100*index, animated: true})
    }

    render() {
        return (
            <View style={styles.pageWrapper}>
                <Header />
                <EventHeader 
                    data={CAROUSEL_DATA} 
                    activeSlide={this.state.currentEvent} 
                    headerContainer={ref => this.headerContainer = ref}
                />
                <EventCarousel 
                    data={CAROUSEL_DATA} 
                    onSnapToItem={this.onSnapToItem} 
                />
                <Footer />
            </View>
        );
    }
}
