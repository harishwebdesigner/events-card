import React from 'react-native';

const styles = React.StyleSheet.create({
    footerWrapper:{
        flexDirection: 'row',
        paddingHorizontal: 40,
        paddingBottom: 20,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    hamBurgerIcon:{
        width: 30,
        height: 30
    },
    createText:{
        fontSize: 14,
        color: '#000000'
    }
});

export default styles;