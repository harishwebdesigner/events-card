import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, Dimensions } from 'react-native';
import styles from './EventCarouselCss';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from './SlideWidthStyle';

export default class EventCarousel extends React.PureComponent {


    _renderItem ({item, index}) {
    	console.log(item.illustration)
        return (
            <View activeOpacity={0.96} style={styles.slide}>
            	<View style={styles.imgWrapper}>
                	<Image style={styles.slideImage} source={{uri: item.illustration}} />
                </View>
            </View>
        );
    }



    render () {
    	return (
			<View style={styles.carouselContainer}>
				<Carousel
					ref={(c) => { this._carousel = c; }}
					data={this.props.data}
					renderItem={this._renderItem}
					sliderWidth={sliderWidth}
					itemWidth={itemWidth}
					loop={true}
					loopClonesPerSide={20}
					layout={'stack'} 
                    inactiveSlideScale={0.02}
					layoutCardOffset={24}
					containerCustomStyle={styles.slider}
					contentContainerCustomStyle={styles.sliderContentContainer}
                    onSnapToItem={this.props.onSnapToItem}
				/>
            </View>
        );
    }
}