import React from 'react-native';

const styles = React.StyleSheet.create({
    eventHeaderWrapper:{
        paddingHorizontal: 20,
        height: 100
    },
    titleView:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title:{
        fontWeight: 'bold',
        fontSize: 30,
        color: '#000000',
    },
    date:{
        fontSize: 12,
        color: '#9b9b9b',
    },
    locationView:{
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5
    },
    locationIcon:{
        height: 20,
        width: 20,
        marginRight: 10
    }
});

export default styles;