import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import styles from './HeaderCss';

export default class Header extends React.PureComponent {
    render() {
        return (
            <View style={styles.headerWrapper}>
                <View style={styles.leftView}>
                    <TouchableOpacity style={styles.backBtn}>
                        <Image style={styles.backIcon} source={require('../../Assets/Images/back.png')} />
                    </TouchableOpacity>
                    <Text style={styles.headerTitle}>Events</Text>
                </View>
                <TouchableOpacity style={styles.searchBtn}>
                    <Image style={styles.searchIcon} source={require('../../Assets/Images/search.png')} />
                </TouchableOpacity>
            </View>
        );
    }
}
