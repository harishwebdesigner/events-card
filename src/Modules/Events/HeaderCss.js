import React from 'react-native';

const styles = React.StyleSheet.create({
    headerWrapper:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    leftView:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerTitle:{
        fontSize: 16,
        color: '#000000',
    },
    backBtn:{
        marginRight: 20
    },
    backIcon:{
        resizeMode: 'contain',
        width: 20
    },
    searchBtn:{
        alignSelf: 'flex-end',
    },
    searchIcon:{
        resizeMode: 'contain',
        width: 20
    }
});

export default styles;