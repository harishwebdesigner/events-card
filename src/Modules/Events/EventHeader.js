import React, { Component } from 'react';
import { Text, View, Image, ScrollView, TouchableOpacity } from 'react-native';
import styles from './EventHeaderCss';
import _ from 'lodash';

export default class EventHeader extends React.PureComponent {

    constructor(props){
        super(props);

    }


    render() {
        return (
            <ScrollView 
                ref={this.props.headerContainer} 
                scrollEnabled={false} 
                showsVerticalScrollIndicator={false}
                scrollEventThrottle={20}
                snapToInterval={20}
            >
                {
                    _.map(this.props.data, (header, i)=>{
                        return(
                            <View key={i} style={styles.eventHeaderWrapper}>
                                <View style={styles.titleView}>
                                    <Text style={styles.title}>{header.title}</Text>
                                    <Text style={styles.date}>{header.eventTime}</Text>
                                </View>
                                <View style={styles.locationView}>
                                    <Image style={styles.locationIcon} source={require('../../Assets/Images/location.png')} />
                                    <Text style={styles.date}>{header.location}</Text>
                                </View>
                            </View>
                        );
                    })
                }
            </ScrollView>
        );
    }
}
