import React from 'react-native';
import { Dimensions } from 'react-native';

const styles = React.StyleSheet.create({
    slider: {
        marginTop: 15,
        overflow: 'hidden',
        transform: [{ scaleX: -1 }]
    },
    slide:{
        paddingHorizontal: 20,
        paddingBottom: 20,
    },
    imgWrapper:{
        elevation: 10,
        backgroundColor: 'rgba(0,0,0,0)',
        borderRadius: 10,
        overflow: 'hidden',
    },
    slideImage:{
        borderRadius: 10,
        width: '100%',
        height: Dimensions.get('window').height - 270,
        transform: [{ scaleX: -1 }]
    },
    sliderContentContainer: {
        paddingVertical: 10 // for custom animation
    },
});

export default styles;